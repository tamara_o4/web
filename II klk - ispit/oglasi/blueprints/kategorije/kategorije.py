import flask
from flask.blueprints import Blueprint

from utils.db import mysql

kategorije_blueprint = Blueprint("kategorije_blueprint", __name__)

@kategorije_blueprint.route("/kategorije", methods=["GET"])
def dobavi_kategorije():
    cursor = mysql.get_db().cursor() 
    cursor.execute("SELECT * FROM kategorija") 
    servisi = cursor.fetchall() 
    return flask.jsonify(servisi) 

@kategorije_blueprint.route("/kategorije/<int:id_kategorije>")
def dobavi_kategoriju(id_kategorije, methods=["GET"]):
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM kategorija WHERE id=%s", (id_kategorije,))
    servisi = cursor.fetchone() 
    if servisi is not None:
        return flask.jsonify(servisi) 
    else:
        print("DOSLO JE DO GRESKE PRILIKOM DOBAVLJANJA KORISNIKA")
        return "", 404 

@kategorije_blueprint.route("/kategorije", methods=["POST"])
def dodaj_kategoriju():
    db = mysql.get_db() 
    cursor = db.cursor()    
    print(flask.request.json)
    cursor.execute("INSERT INTO kategorija(naziv) VALUES(%(naziv)s)", flask.request.json)
  
    db.commit()
    return flask.jsonify(flask.request.json), 201 

@kategorije_blueprint.route("/kategorije/<int:id_kategorije>", methods=["PUT"])
def izmeni_kategoriju(id_kategorije):
    db = mysql.get_db()
    cursor = db.cursor()
    data = flask.request.json
    data["id"] = id_kategorije 
    cursor.execute("UPDATE kategorija SET naziv=%(naziv)s WHERE id=%(id)s", data)
    db.commit()
    return "", 200


@kategorije_blueprint.route("/kategorije/<int:id_kategorije>", methods=["DELETE"])
def ukloni_kategoriju(id_kategorije):
    print(id_kategorije)
    db = mysql.get_db()
    cursor = db.cursor()

    cursor.execute("DELETE FROM kategorija WHERE id=%s", (id_kategorije,))
    db.commit()
    return "", 204 
