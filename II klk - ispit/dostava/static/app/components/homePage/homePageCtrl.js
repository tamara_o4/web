(function (angular) {
    let app = angular.module("app").controller("homePageCtrl", [
      "$http","$stateParams",
      function ($http, $stateParams) {
        let that = this;
        let noviKorisnik = {
          ime: "",
          prezime: ""
        }
  
        this.dobaviKorisnike = function () {
          $http.get("/api/korisnici").then(
            function (response) {
              that.korisnici = response.data
            },
            function (reason) {
              console.log(reason)
            }
          )
        }
  
        this.obrisiKorisnika = function (id) {
          $http.delete("/api/korisnici/" + id).then(
            function (response) {
              that.dobaviKorisnike()
            },
            function (reason) {
              console.log(reason)
            }
          )
        }
  
        that.dobaviKorisnike()
  
  
  
      }
    ])
  })(angular)
  