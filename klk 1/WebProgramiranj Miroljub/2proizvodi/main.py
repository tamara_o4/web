import flask
from flask import Flask

app = Flask(__name__,static_url_path="")

proizvodi = [
    {
        "naziv":"Jabuka",
        "dobavljac":"0",
        "jm":"kg",
        "cena":100,
        "obrisan":False
    }
]

dobavljaci = [
    {
        "id":"0",
        "adresa":"srbija",
        "naziv":"idea"
    },
    {
        "id":"1",
        "adresa":"rusija",
        "naziv":"s400"
    }
]

proizvodi_na_stanju = [
    {
        "proizvod":"Jabuka",
        "kolicina":100
    }
]

@app.route("/")
def tabela_proizvoda():
    return flask.render_template("proizvodi.html",proizvodi=proizvodi,dobavljaci=dobavljaci)

@app.route("/brisanje/<int:index>")
def brisanje(index):
    proizvodi[index]["obrisan"] = not proizvodi[index]["obrisan"]
    return flask.redirect("/")




@app.route("/dodavanjeProizvoda")
def forma_za_dodavanje_porizvoda():
    return flask.render_template("addProizvod.html",dobavljaci=dobavljaci)

@app.route("/reloadProizvodi",methods=["POST"])
def izmenjen_prikaz_proizvoda():
    proizvodi.append({
        "naziv":flask.request.form["naziv"],
        "dobavljac":flask.request.form["dobavljac"],
        "jm":flask.request.form["jm"],
        "cena":flask.request.form["cena"],
        "obrisan":False
    })
    return flask.redirect("/")




@app.route("/dobavljaci")
def forma_za_dodavanje_dobavljaca():
    return flask.render_template("addDobavljaca.html",dobavljaci=dobavljaci)

@app.route("/dodanDobavljac",methods=["POST"])
def dodat_dobavljac():
    dobavljaci.append(
        {
            "id":flask.request.form["id"],
            "adresa":flask.request.form["adresa"],
            "naziv":flask.request.form["naziv"]
        }
    )        
    print("Dobavljac ", flask.request.form["naziv"], " dodat")
    return flask.redirect("/dobavljaci")




@app.route("/proizvodiNaStanju")
def prikaz_proizvodi_na_stanju():
    return flask.render_template("addPNS.html",proizvodi_na_stanju=proizvodi_na_stanju,proizvodi=proizvodi)

@app.route("/dodajPNS",methods=["POST"])
def rld_prikaz_pns():
    postoji = False
    for p in proizvodi_na_stanju:
        if p["proizvod"] == flask.request.form["proizvod"]:
            postoji = True
            p["kolicina"] = p["kolicina"] + int(flask.request.form["kolicina"])
    if postoji == False:
        proizvodi_na_stanju.append({
            "proizvod":flask.request.form["proizvod"],
            "kolicina":int(flask.request.form["kolicina"])
        })
    return flask.redirect("/proizvodiNaStanju")



@app.route("/kupovina")
def kupovina():
    return flask.render_template("kupovina.html",proizvodi_na_stanju=proizvodi_na_stanju)

@app.route("/racun",methods=["POST"])
def racun():
    konacan_racun = {
    "proizvod":flask.request.form["proizvod"],
    "kolicina":flask.request.form["kolicina"]
    }
    for p in proizvodi_na_stanju:
        if p["proizvod"] == flask.request.form["proizvod"]:
            if p["kolicina"] >= int(flask.request.form["kolicina"]):
                p["kolicina"] = p["kolicina"] - int(flask.request.form["kolicina"])
                break;
            else:
                return flask.render_template("greska.html",konacan_racun=konacan_racun)
    return flask.render_template("racun.html",konacan_racun=konacan_racun,proizvodi=proizvodi)


if __name__ == "__main__":
    app.run("0.0.0.0",5000,threaded=True)