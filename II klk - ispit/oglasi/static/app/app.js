(function(angular){
    // Kreiranje modula pod nazivom app koji kao zavisnost ima ui router modul.
    // U slucaju da je modul vec kreiran, umesto kreiranja novog, bice
    // dobavljen postojeci modul.
    var app = angular.module("app", ["ui.router"]);

    app.config(["$stateProvider", "$urlRouterProvider", function($stateProvider, $urlRouterProvider) {
        // Stanje se registruje preko $stateProvider servisa, pozivom
        // metode state kojoj se prosledjuje state objekat.
        $stateProvider.state({
            name: "home", // Naziv stanja, mora biti zadat.
            url: "/", // URL kojim se prelazi na zadato stanje.
            templateUrl: "app/components/homePage/homePage.tpl.html", // URL do sablona koji se koristi za zadato stanje.
            controller: "homePageCtrl", // Kontroler koji se vezuje za sablon u zadatom stanju.
            controllerAs: "ctrl" // Naziv vezanog kontrolera.
        }).state({
            name: "izmeniKorisnika", // Naziv stanja, mora biti zadat.
            url: "/izmeniKorisnika/{id}", // URL kojim se prelazi na zadato stanje.
            templateUrl: "app/components/izmeniKorisnika/izmeniKorisnika.tpl.html", // URL do sablona koji se koristi za zadato stanje.
            controller: "izmeniKorisnikaCtrl", // Kontroler koji se vezuje za sablon u zadatom stanju.
            controllerAs: "ctrl" // Naziv vezanog kontrolera.
        })
        .state({
            name: "dodajOglas",
            url: "/dodajOglas",
            templateUrl: "app/components/dodajOglas/dodajOglas.tpl.html",
            controller: "dodajOglasCtrl",
            controllerAs: "ctrl"
        })
        .state({
            name: "dodajKorisnika",
            url: "/dodajKorisnika",
            templateUrl: "app/components/dodajKorisnika/dodajKorisnika.tpl.html",
            controller: "dodajKorisnikaCtrl",
            controllerAs: "ctrl"
        })
        .state({
            name: "dodajKategoriju",
            url: "/dodajKategoriju",
            templateUrl: "app/components/dodajKategoriju/dodajKategoriju.tpl.html",
            controller: "dodajKategorijuCtrl",
            controllerAs: "ctrl"
        })
        .state({
            name: "izmeniKategoriju", // Naziv stanja, mora biti zadat.
            url: "/izmeniKategoriju/{id}", // URL kojim se prelazi na zadato stanje.
            templateUrl: "app/components/izmeniKategoriju/izmeniKategoriju.tpl.html", // URL do sablona koji se koristi za zadato stanje.
            controller: "izmeniKategorijuCtrl", // Kontroler koji se vezuje za sablon u zadatom stanju.
            controllerAs: "ctrl" // Naziv vezanog kontrolera.
        })
        .state({
            name: "izmeniOglas", // Naziv stanja, mora biti zadat.
            url: "/izmeniOglas/{id}", // URL kojim se prelazi na zadato stanje.
            templateUrl: "app/components/izmeniOglas/izmeniOglas.tpl.html", // URL do sablona koji se koristi za zadato stanje.
            controller: "izmeniOglasCtrl", // Kontroler koji se vezuje za sablon u zadatom stanju.
            controllerAs: "ctrl" // Naziv vezanog kontrolera.
        })
        .state({
            name: "pretragaOglasa",
            url: "/pretragaOglasa",
            templateUrl: "app/components/pretragaOglasa/pretragaOglasa.tpl.html",
            controller: "pretragaOglasaCtrl",
            controllerAs: "ctrl"
        })
        /*.state({
            name: "dodavanjeProizvoda",
            url: "/proizvodForma",
            templateUrl: "app/components/proizvodForma/proizvodForma.tpl.html",
            controller: "ProizvodFormaCtrl",
            controllerAs: "pctrl"
        }).state({
            name: "proizvodi",
            url: "/proizvodi",
            templateUrl: "app/components/proizvodi/proizvodi.tpl.html",
            controller: "ProizvodiCtrl",
            controllerAs: "pctrl"
        }).state({
            name: "proizvod",
            url: "/proizvodi/{id}", // Ovaj URL je razlicit od URL-a za pristup svim proizvodima
                                    // jer sadrzi parametar id. Samo ukoliko se navede i parametar
                                    // dolazi do poklapanja sa ovim URL-om i do aktiviranja stanja.
            templateUrl: "app/components/proizvod/proizvod.tpl.html",
            controller: "ProizvodCtrl",
            controllerAs: "pctrl"
        }).state({
            name: "izmenaProizvoda",
            url: "/proizvodForma/{id}",
            templateUrl: "app/components/proizvodForma/proizvodForma.tpl.html",
            controller: "ProizvodFormaCtrl", // Isti kontroler se moze koristiti vise puta.
            controllerAs: "pctrl"
        }).state({
            name: "login",
            url: "/login",
            templateUrl: "app/components/login/login.tpl.html",
            controller: "loginCtrl", // Isti kontroler se moze koristiti vise puta.
            controllerAs: "lctrl"
        })*/;

        // Ukoliko zadati url ne pokazuje ni na jedno stanje
        // vrsi se preusmeravajne na url zadat pozivom metode
        // otherwise nad $urlRouterProvider servisom.
        $urlRouterProvider.otherwise("/")
    }])
})(angular);
