(function(angular){

    var app = angular.module("app");


    app.controller("korisniciCtrl", ["$http" , "$state", function($http, $state) {
        var that = this; 

        this.korisnici = []; 
        this.tipovi = [];
        this.dobaviKorisnike= function() {
            $http.get("api/korisnici").then(function(result){
                console.log(result);
                that.korisnici = result.data;
            },
            function(reason) {
                console.log(reason);
            });
        }
        this.dobaviTipove= function() {
            $http.get("api/tipovi").then(function(result){
                console.log(result);
                that.tipovi = result.data;
            },
            function(reason) {
                console.log(reason);
            });
        }
        this.ukloniKorisnika = function(id) {
            $http.delete("api/korisnici/" + id).then(function(response){
                console.log(response);
                that.dobaviLekare();
            },
            function(reason){
                console.log(reason);
            });
        }
        this.ukloniTipove = function(id) {
            $http.delete("api/tipovi/" + id).then(function(response){
                console.log(response);
                that.dobaviTipove();
            },
            function(reason){
                console.log(reason);
            });
        }
        this.dobaviKorisnike();
        this.dobaviTipove();
    }]);
})(angular);