(function (angular) {
    var app = angular.module("app");
    app.controller("korisnikFormaCtrl", ["$http", "$state", "$stateParams", function ($http, $state, $stateParams) {
        var that = this;

        this.noviKorisnik = {
            "korisnicko_ime": "",
            "ime": "",
            "prezime": ""
        };

        // Funkcija za dobavljanje svih kategorija.

        this.dobaviKorisnika = function(id) {
            $http.get("api/korisnici/" + id).then(function(result){
                that.noviKorisnik = result.data;
            }, function(reason) {
                console.log(reason);
            });
        }

        // Funkcija za dodavanje korisnika.
        this.dodajKorisnika = function () {
            // Upucuje se post zahtev na relativni URL api/korisnici.
            // Uz url funkciji post se prosledjuje i objekat koji
            // treba da se prosledi u telu zahteva. Angular ce automatski
            // ovaj objekat pretvoriti u JSON format i tako ga proslediti
            // u telu zahteva.
            $http.post("api/korisnici", this.noviKorisnik).then(function (response) {
                console.log(response);
                // Pri uspesnom dodavanju korisnika precice se na stranicu za prikaz
                // svih korisnika. Prelazak na stranicu moze se postici pozivom metode
                // go iz $state servisa. Ovoj metodi se prosledjuje naziv stanja na
                // koje je potrebno preci i po potrebi objekat koji sadrzi vrednosti
                // parametara koje treba proslediti pri prelasku u navedeno stanje.
                $state.go("home")
            }, function (reason) {
                console.log(reason);
                // Ukoliko neprijavljeni korisnik pokusa da doda lekar
                // prikazuje se stranica za prijavu.
            });
        }
        this.izmeniKorisnika = function(id) {
            $http.put("api/korisnici/" + id, that.noviKorisnik).then(function(response) {
                console.log(response)
                // Prelazak na stanje sa parametrizovanim ULR-om pri cemu
                // se vrednost parametra prosledjuje prilikom poziva
                // metode go.
            }, function(reason) {
                console.log(reason);
                // Ukoliko neprijavljeni korisnik pokusa da izmeni proizvod
                // prikazuje se stranica za prijavu.
            });
        }

        this.sacuvaj = function() {
            if($stateParams["id"]){
            this.izmeniKorisnika($stateParams["id"], that.korisnik);
            }else{
                this.dodajKorisnika()
            }
            
        }

        if($stateParams["id"]) {
            this.dobaviKorisnika($stateParams["id"]);
        }

        if($stateParams["id"]) {
            this.dobaviKorisnika($stateParams["id"]);
        }
    }]);
})(angular);