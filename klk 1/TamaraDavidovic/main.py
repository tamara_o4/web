import flask
from flask import Flask
from flask import request

app = Flask(__name__, static_url_path="")

nekretnine = [
    {
        "naziv": "Stan1",
        "adresa": "Bulvar Oslobodjenja 7",
        "godina": "2016.",
        "sobe": 1,
        "povrsina": "25m2",
        "cena": 32000,
        "status": "na prodaji",
        "obrisan": False
    },
    {
        "naziv": "Stan2",
        "adresa": "Bulvar Oslobodjenja 27",
        "godina": "2000.",
        "sobe": 4,
        "povrsina": "95m2",
        "cena": 102000,
        "status": "na prodaji",
        "obrisan": False
    }
]

kupovine = []

#putanja koja prikazuje pocetnu stranu sa tabelom nekretnina
@app.route("/")
def tabela_nekretnina():
    return flask.render_template("index.html", nekretnine=nekretnine)
    
#putanja na koju ide kada brise nekretninu nakon cega ide na pocetnu
@app.route("/brisanje/<int:index>")
def brisanje(index):
    nekretnine[index]["obrisan"] = not nekretnine[index]["obrisan"]
    return flask.redirect("/")

#putanja ka formi za dodavanje nekretnine
@app.route("/dodavanjeNekretnine")
def dodavanje_nekretnine():
    return flask.render_template("addNekretninu.html")

#putanja koja prikazuje nakon dodavanja tabelu nekretnina
@app.route("/izmenjenPrikaz", methods=["post"])
def izmenjen_prikaz():
    nekretnine.append({
        "naziv": flask.request.form["naziv"],
        "adresa": flask.request.form["adresa"],
        "godina": flask.request.form["godina"],
        "sobe": flask.request.form["sobe"],
        "povrsina": flask.request.form["povrsina"],
        "cena": flask.request.form["cena"],
        "status": flask.request.form["status"],
        "obrisan": False
    })
    return flask.redirect("/")

#izmena 
@app.route("/izmeniFormu/<int:index>")
def izmeniFormu(index):
    return flask.render_template("izmena.html", nekretnine=nekretnine[index], index=index)

@app.route("/izmeni/<int:index>", methods=["POST"])
def izmena(index):
    nekretnine[index]={
        "naziv": flask.request.form["naziv"],
        "adresa": flask.request.form["adresa"],
        "godina": flask.request.form["godina"],
        "sobe": flask.request.form["sobe"],
        "povrsina": flask.request.form["povrsina"],
        "cena": flask.request.form["cena"],
        "status": flask.request.form["status"],
        "obrisan": False
    }
    return flask.redirect("/")

#kupovina
@app.route("/kupovinaForma/<int:index>")
def kupovinaForma(index):
    return flask.render_template("kupovina.html", nekretnine=nekretnine[index], index=index)


@app.route("/kupi/<int:index>", methods=["POST"])
def kupi(index):
    kupovine.append({
        "naziv": nekretnine[index]["naziv"],
        "mbr": flask.request.form["mbr"],
        "brtelefona": flask.request.form["brtelefona"],
        "visinaponude": flask.request.form["visinaponude"],
        "stanje": "u obradi"
    })
    return flask.redirect("/")

#prikaz svih kupovina
@app.route("/statistikaKupovina")
def statistika():
    return flask.render_template("statistikaKupovina.html", nekretnine=nekretnine, kupovine=kupovine)

#izmena 
@app.route("/izmenaStanja/<int:index>")
def izmeniStanje(index):
    return flask.render_template("stanje.html", kupovine=kupovine[index], index=index)

@app.route("/stanje/<int:index>", methods=["POST"])
def stanje(index):
    kupovine[index]={
        "naziv": nekretnine[index]["naziv"],
        "mbr": kupovine[index]["mbr"],
        "brtelefona": kupovine[index]["brtelefona"],
        "visinaponude": kupovine[index]["visinaponude"],
        "stanje": flask.request.form["stanje"],  
    }
    return flask.redirect("/")

if __name__ == "__main__":
    app.debug = True
    app.run("0.0.0.0", 5000, threaded=True)
