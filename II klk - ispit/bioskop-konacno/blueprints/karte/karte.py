import flask
from flask.blueprints import Blueprint

from utils.db import mysql

karte_blueprint = Blueprint("karte_blueprint", __name__)

@karte_blueprint.route("/karte", methods=["GET"])
def dobavi_korisnike():
    cursor = mysql.get_db().cursor() 
    cursor.execute("SELECT * FROM karta") 
    karte = cursor.fetchall() 
    return flask.jsonify(karte) 

@karte_blueprint.route("/karte/<int:id_karte>")
def dobavi_korisnika(id_karte, methods=["GET"]):
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM karta WHERE id=%s", (id_karte,))
    proizvod = cursor.fetchone() 
    if proizvod is not None:
        return flask.jsonify(proizvod) 
    else:
        print("DOSLO JE DO GRESKE PRILIKOM DOBAVLJANJA KORISNIKA")
        return "", 404 

@karte_blueprint.route("/karte", methods=["POST"])
def dodaj_korisnika():
    db = mysql.get_db() 
    cursor = db.cursor() 

    print(flask.request.json)
    cursor.execute("INSERT INTO karta(korisnik_id, projekcija_id, datum_kupovine,cena) VALUES(%(korisnik_id)s, %(projekcija_id)s, %(datum_kupovine)s, %(cena)s)", flask.request.json)
  
    db.commit()
    return flask.jsonify(flask.request.json), 201 

@karte_blueprint.route("/karte/<int:id_karte>", methods=["PUT"])
def izmeni_korisnika(id_karte):
   
    db = mysql.get_db()
    cursor = db.cursor()
    data = flask.request.json
    data["id"] = id_karte 
    cursor.execute("UPDATE karta SET korisnik_id=%(korisnik_id)s, projekcija_id=%(projekcija_id)s, datum_kupovine=%(datum_kupovine)s, cena=%(cena)s WHERE id=%(id)s", data)
    db.commit()
    return "", 200


@karte_blueprint.route("/karte/<int:id_karte>", methods=["DELETE"])
def ukloni_korisnika(id_karte):
    print(id_karte)
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("DELETE FROM karta WHERE id=%s", (id_karte,))
    db.commit()
    return "", 204 
