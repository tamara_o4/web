(function (angular) {
    let app = angular.module("app").controller("izmeniKategorijuCtrl", [
      "$http",
      "$state",
      "$stateParams",
      function ($http, $state, $stateParams) {
        let that = this;
  
        let novaKategorija = {
          "naziv": "",
        };
  
        this.dobaviKategoriju = function (id) {
          $http.get("/api/kategorije/" + id).then(
            function (response) {
              that.novaKategorija = response.data;
            },
            function (reason) {
              console.log(reason);
            }
          );
        };
  
        this.izmeniKategoriju = function (id) {
          $http.put("/api/kategorije/" + id, that.novaKategorija).then(
            function (response) {
              $state.go("home");
            },
            function (reason) {
              console.log(reason);
            }
          );
        };
  
        this.wrapperFunction = function () {
          that.izmeniKategoriju($stateParams["id"]);
        };
  
        that.dobaviKategoriju($stateParams["id"]);
      },
    ]);
  })(angular);
  