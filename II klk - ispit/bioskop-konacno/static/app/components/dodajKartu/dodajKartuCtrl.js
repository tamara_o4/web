(function (angular) {
    let app = angular.module("app").controller("dodajKartuCtrl", [
        "$http",
        "$state",
        function ($http, $state) {
            let that = this;
            let novaProjekcija = {
                film_id: "",
                termin: "",
                cena: "",
            }

            let novaKarta = {
                korisnik_id: "",
                projekcija_id: "",
                datum_kupovine: new Date(),
                cena: novaProjekcija.cena,
            }

            this.dobaviProjekciju = function (id) {
                $http.get("/api/projekcije/" + id).then(
                    function (result) {
                        that.novaProjekcija = result.data
                    },
                    function (reason) {
                        console.log(reason);
                    }
                )
            }

            this.dobaviProjekcije = function () {
                $http.get("/api/projekcije").then(
                    function (response) {
                        console.log();
                        that.projekcije = response.data;
                    },
                    function (reason) {
                        console.log(reason)
                    }
                )
            }

            this.dobaviKorisnike = function () {
                $http.get("/api/korisnici").then(
                    function (response) {
                        that.korisnici = response.data
                    },
                    function (reason) {
                        console.log(reason)
                    }
                )
            }

            this.dodajKartu = function () {
                $http.post("/api/karte", that.novaKarta).then(
                    function (response) {
                        $state.go("prikaziKarte")
                    },
                    function (reason) {
                        console.log(reason)
                    }
                )
            }

            this.wrapperFunction = function () {
                that.dobaviProjekciju(that.novaKarta.projekcija_id)
                that.dodajKartu();
                console.log(that.novaKarta.datum_kupovine)
                console.log(that.novaKarta.cena)
            }
            this.dobaviProjekcije()
            this.dobaviKorisnike()
        },
    ]);
})(angular);