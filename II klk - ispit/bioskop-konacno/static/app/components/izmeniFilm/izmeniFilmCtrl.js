(function (angular) {
    let app = angular.module("app").controller("izmeniFilmCtrl", ["$http","$state","$stateParams",
        function ($http, $state, $stateParams) {
            let that = this;

            let noviFilm = {
                naziv: "",
                zanr: "",
                reziser: "",
                premijera: "",
            }

            this.srediDatum = function (callback, param) {
                //mysqlDateFormat = that.noviFilm.premijera.toISOString().slice(0, 10);
                //that.noviFilm.premijera = mysqlDateFormat;
                that.noviFilm.premijera.setDate(that.noviFilm.premijera.getDate() + 1)
                mysqlDateFormat = that.noviFilm.premijera.toISOString().slice(0, 10)
                that.noviFilm.premijera = mysqlDateFormat
                callback(param);
            }

            this.dobaviFilm = function (id) {
                $http.get("/api/filmovi/" + id).then(
                    function (response) {
                        that.noviFilm = response.data
                        console.log(that.noviFilm)
                    },
                    function (reason) {
                        console.log(reason);
                    }
                )
            }

            this.izmeniFilm = function (id) {
                $http.put("/api/filmovi/" + id, that.noviFilm).then(
                    function (response) {
                        $state.go("home")
                    },
                    function (reason) {
                        console.log(reason)
                    }
                )
            }

            this.wrapperFunction = function () {
                that.srediDatum(that.izmeniFilm, $stateParams["id"])
            }

            that.dobaviFilm($stateParams["id"])
        }
    ])
})(angular)