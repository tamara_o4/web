import flask
from flask.blueprints import Blueprint

from utils.db import mysql

projekcije_blueprint = Blueprint("projekcije_blueprint", __name__)

@projekcije_blueprint.route("/projekcije", methods=["GET"])
def dobavi_korisnike():
    cursor = mysql.get_db().cursor() 
    cursor.execute("SELECT * FROM projekcija") 
    projekcije = cursor.fetchall() 
    return flask.jsonify(projekcije) 

@projekcije_blueprint.route("/projekcije/<int:id_projekcije>")
def dobavi_korisnika(id_projekcije, methods=["GET"]):
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM projekcija WHERE id=%s", (id_projekcije,))
    proizvod = cursor.fetchone() 
    if proizvod is not None:
        return flask.jsonify(proizvod) 
    else:
        print("DOSLO JE DO GRESKE PRILIKOM DOBAVLJANJA KORISNIKA")
        return "", 404 

@projekcije_blueprint.route("/projekcije", methods=["POST"])
def dodaj_korisnika():
    db = mysql.get_db() 
    cursor = db.cursor() 

    print(flask.request.json)
    cursor.execute("INSERT INTO projekcija(film_id, termin, cena) VALUES(%(film_id)s, %(termin)s, %(cena)s)", flask.request.json)
  
    db.commit()
    return flask.jsonify(flask.request.json), 201 

@projekcije_blueprint.route("/projekcije/<int:id_projekcije>", methods=["PUT"])
def izmeni_korisnika(id_projekcije):
   
    db = mysql.get_db()
    cursor = db.cursor()
    data = flask.request.json
    data["id"] = id_projekcije 
    cursor.execute("UPDATE projekcija SET film_id=%(film_id)s, termin=%(termin)s, cena=%(cena)s WHERE id=%(id)s", data)
    db.commit()
    return "", 200


@projekcije_blueprint.route("/projekcije/<int:id_projekcije>", methods=["DELETE"])
def ukloni_korisnika(id_projekcije):
    print(id_projekcije)
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("DELETE FROM projekcija WHERE id=%s", (id_projekcije,))
    db.commit()
    return "", 204 
