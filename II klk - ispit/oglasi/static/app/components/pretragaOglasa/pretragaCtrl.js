(function (angular) {
    let app = angular.module("app").controller("pretragaOglasaCtrl", [
      "$http","$stateParams",
      function ($http, $stateParams) {
        //korisnikFormaCtrl
        let that = this;
        let noviOglas ={
          kategorija_id: "",
          korisnik_id: "",
          naslov: "",
          sadrzaj: "",
          datum_objave: ""
        }
        let pretraga = ""
        let pretraga1 = ""
        let pretraga2 = ""

        this.dobaviKorisnike = function () {
          $http.get("/api/korisnici").then(
            function (response) {
              that.korisnici = response.data
            },
            function (reason) {
              console.log(reason)
            }
          )
        }

//======================== oglasi

        this.dobaviOglase = function () {
          $http.get("/api/oglasi").then(
            function (response) {
              that.oglasi = response.data
            },
            function (reason) {
              console.log(reason)
            }
          )
        }

        this.obrisiOglas = function (id) {
          $http.delete("/api/oglasi/" + id).then(
            function (response) {
              that.dobaviOglase()
            },
            function (reason) {
              console.log(reason)
            }
          )
        }

        //==========================
        this.dobaviKategorije = function () {
          $http.get("/api/kategorije").then(
            function (response) {
              that.kategorije = response.data
            },
            function (reason) {
              console.log(reason)
            }
          )
        }

        that.dobaviKorisnike()
        that.dobaviOglase()
        that.dobaviKategorije()
  
  
      }
    ])
  })(angular)
