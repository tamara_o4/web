import flask
from flask import Flask

app = Flask(__name__)

@app.route("/")

def index():
    return """
    <html>
        <head>
            <title>Registracija</title>
        </head>
        <body>
            <!--Atribut action - odredjuje gde ce biti upucen zahtev iz forme-->
            <!--Atribut method - odredjuje koji tip zahteva ce biti upucen, u ovom slucaju to je GET -->
        <form action="/registracija" method="GET">
            <div>
                <label>Ime: </label>
            </div>
            <div>
                <!--Atribut name - ce biti upotrebljen ya iydavanje vrednosti polja na strani servera-->
                <input type="text" id="ime">
            </div>
            <div>
                <label>Prezime: </label>
            </div>
            <div>
                <input type="text" id="prezime">
            </div>
            <div>
                <label>Korisnicko ime: </label>
            </div>
            <div>
                <input type="text" id="korIme">
            </div>
            <div>
                <label>Lozinka: </label>
            </div>
            <div>
                <input type="password" id="lozinkka">
            </div>
            <div>
                <label>Ponovljena lozinka: </label>
            </div>
            <div>
                <input type="password" id="ponLozinka">
            </div>
            <div>
                <label>Datum rodjenja: </label>
            </div>
            <div>
                <input type="date" id="datRodj">
            </div>
            <div>
                <input type="submit" value="Registruj se">
            </div>
        </form>
        </body>
    </html>
        """
app.run("0.0.0.0", 5000, threaded=True)