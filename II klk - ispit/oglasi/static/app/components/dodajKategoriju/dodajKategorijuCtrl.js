(function (angular) {
    let app = angular.module("app").controller("dodajKategorijuCtrl", [
      "$http",
      "$state",
      function ($http, $state) {
        let that = this
  
        let novaKategorija = {
          "naziv": "",
        }

  
        this.dodajKategoriju = function () {
          $http.post("/api/kategorije", that.novaKategorija).then(
            function (response) {
              $state.go("home")
            },
            function (reason) {
              console.log(reason)
            }
          )
        }
        this.wrapperFunction = function () {
          that.dodajKategoriju();
        }

      }
    ])
  })(angular)
  