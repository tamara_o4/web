import flask
from flask import Flask
from flask import request

app = Flask(__name__, static_url_path="")

proizvodi = [
    {
        "naziv": "Cokolada",
        "dobavljac": "Dobavljac 1",
        "jedinica_mere": "gram",
        "cena": 200,
        "obrisan": False
    },
    {    
        "naziv": "Cokoladaa",
        "dobavljac": "Dobavljac 1",
        "jedinica_mere": "gram",
        "cena": 200,
        "obrisan": False
    }
]

dobavljaci = [
    {
        "id" : "s123",
        "adresa" : "Bulevar Oslobodjenja 7, Novi Sad",
        "naziv" : "Dobavlvljac 1"
    }
]

proizvodi_na_stanju = [
    {
        "proizvod" : "Cokolada",
        "kolicina" : 200
    }
]

#putanja koja prikazuje pocetnu stranu sa tabelom proizvoda
@app.route("/")
def tabela_proizvodi():
    return flask.render_template("tabelaProizvoda.html", proizvodi=proizvodi)

#putanja na koju ide kada brise dobavljaca nakon cega ide na pocetnu
@app.route("/brisanje/<int:index>")
def brisanje(index):
    proizvodi[index]["obrisan"] = not proizvodi[index]["obrisan"]
    return flask.redirect("/")

@app.route("/dodavanjeProizvoda")
def dodavanje_proizvoda():
    return flask.render_template("dodavanjeProizvoda.html")


if __name__ == "__main__":
    app.run("0.0.0.0",5000,threaded=True)
