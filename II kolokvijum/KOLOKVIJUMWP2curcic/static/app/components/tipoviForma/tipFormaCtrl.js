(function (angular) {
    var app = angular.module("app");
    app.controller("tipFormaCtrl", ["$http", "$state", "$stateParams", function ($http, $state, $stateParams) {
        var that = this;

        this.noviTip = {
            "naziv": "",
            "skraćenica": ""
        };


        this.dobaviTipa = function(id) {
            $http.get("api/tipovi/" + id).then(function(result){
                that.noviTip = result.data;
            }, function(reason) {
                console.log(reason);
            });
        }


        this.dodajTip = function () {
            $http.post("api/tipovi", this.noviTip).then(function (response) {
                console.log(response);
                $state.go("home")
            }, function (reason) {
                console.log(reason);
            });
        }
        this.izmeniTip = function(id) {
            $http.put("api/tipovi/" + id, that.noviTip).then(function(response) {
                console.log(response)
            }, function(reason) {
                console.log(reason);
            });
        }

        this.sacuvaj = function() {     
            if($stateParams["id"]){
            this.izmeniTip($stateParams["id"], that.tip);
            }else{
                this.dodajTip()
            }
            
        }

        if($stateParams["id"]) {
            this.dobaviTipa($stateParams["id"]);
        }

        if($stateParams["id"]) {
            this.dobaviTipa($stateParams["id"]);
        }
    }]);
})(angular);