(function (angular) {
    let app = angular.module("app").controller("dodajFilmCtrl", ["$http","$state",
    function ($http, $state) {
            let that = this

            let noviFilm = {
                naziv: "",
                zanr: "",
                reziser: "",
                premijera: "",
            };

            this.srediDatum = function (callback) {
                mysqlDateFormat = that.noviFilm.premijera.toISOString().slice(0, 10)
                that.noviFilm.premijera = mysqlDateFormat
                callback()
            }

            this.dodajFilm = function () {
                $http.post("/api/filmovi", that.noviFilm).then(
                    function (response) {
                        $state.go("home");
                    },
                    function (reason) {
                        console.log(reason);
                    }
                )
            }

            this.wrapperFunction = function () {
                that.srediDatum(that.dodajFilm)
            }
        }
    ])
})(angular)