import flask
from flask.blueprints import Blueprint

from utils.db import mysql

dogadjaj_blueprint = Blueprint("dogadjaj_blueprint", __name__)
                                                                    
@dogadjaj_blueprint.route("/dogadjaj", methods=["GET"])
def dobavi_dogadjaje():
    cursor = mysql.get_db().cursor() # Dobavljanje instance kursora.
    cursor.execute("SELECT * FROM dogadjaj") # Izvrsavanje upita za dobavljanje svih proizvoda
                                                                # cija kolicina je veca od 0. Ovime je posao filtriranja
                                                                # proizvoda po kolicini izmesten iz generisanja sablona
                                                                # na server baze podataka koji je optimivan za ovakve
                                                                # zadatke.
    dogadjaj = cursor.fetchall() # Dobavljanje svih rezultat prethodni izvrsenog upita.
    return flask.jsonify(dogadjaj) # Vracanje proizvoda u JSON formatu.
                                    # Funkcija jsonify pretvara prosledjeni objekat u JSON format
                                    # i pakuje ga u response objekat u kojem su podesena sva
                                    # neophodna zaglavlja. Upotreba dumps funkcije iz
                                    # JSON modula vratila bi JSON ali ne bi napravila i response
                                    # objekat, koji bi se morao napraviti naknadno rucno.

@dogadjaj_blueprint.route("/dogadjaj/<int:id_dogadjaja>", methods=["GET"])
def dobavi_dogadjaj(id_dogadjaja):
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM dogadjaj WHERE id=%s", (id_dogadjaja,))
    proizvod = cursor.fetchone() # Kako je rezultat upita samo jedna torka dovoljno je
                                 # pozvati metodu fetchone() nad kursorom koja vraca
                                 # sledecu torku koja zadovoljava upit.
    if proizvod is not None:
        return flask.jsonify(proizvod) # Ukoliko je pronadje, proizvod se prosledjuje klijentu.
    else:
        return "", 404 # Ukoliko proizvod nije pronadjen klijent ce dobiti status odgovora 404.
                       # Odnosno podatak da trazeni resurs nije pronadjen.

@dogadjaj_blueprint.route("/dogadjaj", methods=["POST"])
def dodaj_dogadjaja():
    # Provera da li je korisnik prijavljen.
    db = mysql.get_db() # Dobavljanje instance konekcije ka bazi.          
    cursor = db.cursor() # Dobavljanje kursora.

    # Izvrsava se parametrizovani upit sa imenovanim parametrima. Ukoliko se koriste imenovani parametri
    # umesto torkse sa podacima moguce je proslediti recnik koji u se bi sadrzi kljuceve koji odgovaraju
    # imenima parametara. Vrednosti na datim kljucevima ce automatski biti preuzete u istoimenim parametrima.
    # Kako se sadrzaj forme u Flasku predstavlja kao imutabilni recnik koji nasledjuje recnik moguce je umesto
    # obicnog recnika proslediti sam sadrzaj forme. Takodje konverzija podatak nije neophodna jer ce se ispravna
    # konverzija izvrsiti na serveru za upravljanje bazama podataka.
    # Dodatna napomena: Iako je id kolona koja postoji u tabeli proizvod, ona nije navedena prilikom dodavanja
    # jer je ova kolona podesena da bude auto increment, odnosno da se njena vrednost moze automatski generisati.
    # Ovo generisanje ce se desiti samo ukoliko se prilikom izvrsavanja insert naredbe izostavi podesavanje vrednosti
    # za kolonu koja je auto increment ili ako se za njenu vrednost postavi NULL vrednost.
    print(flask.request.json)
    cursor.execute("INSERT INTO dogadjaj(tip_dogadjaja_id, naziv, datum, cena_karte) VALUES(%(tip_dogadjaja)s, %(naziv)s,%(datum)s,%(cena_karte)s)", flask.request.json)
    # Request objekat u flasku sadrzi atribut json, ovaj atribut sadrzace recnik koji je nastao
    # parsiranjem tela zahteva. Telo ce biti parsirano ukoliko je content type bio application/json
    # a recnik ce biti formiran samo ukoliko se u telu nalazio ispravan JSON dokument.
    db.commit() # Upiti se izvrsavaju u transakcijama. Uskladistavanje rezultata transakcije je neophodno rucno potvrditi.
                # Za to se koristi commit() metoda nad konekcijom. Ukoliko se ne pozove commit() transakcija nece biti
                # potvrdjena pa se samim tim rezultat nece uskladistiti u bazu podataka. Vise upita koji zavise
                # jedan od drugo moguce je grupisati u jednu transakciju tako sto se nakon izvrsavanja cele grupe
                # upita pozove commit().
    return flask.jsonify(flask.request.json), 201 # Status kod 201 oznacava uspesno kreiran resurs.

@dogadjaj_blueprint.route("/dogadjaj/<int:id_dogadjaja>", methods=["PUT"])
def izmeni_dogadjaja(id_dogadjaja):
    # Provera da li je korisnik prijavljen.
    
    db = mysql.get_db()
    cursor = db.cursor()
    data = flask.request.json
    data["id"] = id_dogadjaja # Id proizvoda koji treba azurirati preuzima
                              # se iz vrednosti parametra URL-a.
    cursor.execute("UPDATE dogadjaj SET tip_dogadjaja_id=%(tip_dogadjaja_id)s, prezime=%(prezime)s WHERE id=%(id)s", data)
    db.commit()
    return "", 200

# Uklanjanje proizvoda vrsi se po id-ju proizvoda.
# Id ce biti prosledjen kao parametar URL-a.
@dogadjaj_blueprint.route("/dogadjaj/<int:id_dogadjaja>", methods=["DELETE"])
def ukloni_lekara(id_dogadjaja):
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("DELETE FROM dogadjaj WHERE id=%s", (id_dogadjaja,))
    db.commit()
    return "", 204 # Operacija je uspesna ali je telo odgovora prazno.