(function (angular) {
    let app = angular.module("app").controller("homePageCtrl", [
      "$http","$stateParams",
      function ($http, $stateParams) {
        //korisnikFormaCtrl
        let that = this;
        let noviOglas ={
          kategorija_id: "",
          korisnik_id: "",
          naslov: "",
          sadrzaj: "",
          datum_objave: ""
        }
        let pretraga = ""
  
        this.dobaviKorisnike = function () {
          $http.get("/api/korisnici").then(
            function (response) {
              that.korisnici = response.data
            },
            function (reason) {
              console.log(reason)
            }
          )
        }
  
        this.obrisiKorisnika = function (id) {
          $http.delete("/api/korisnici/" + id).then(
            function (response) {
              that.dobaviKorisnike()
            },
            function (reason) {
              console.log(reason)
            }
          )
        }

//======================== oglasi

        this.dobaviOglase = function () {
          $http.get("/api/oglasi").then(
            function (response) {
              that.oglasi = response.data
            },
            function (reason) {
              console.log(reason)
            }
          )
        }

        this.obrisiOglas = function (id) {
          $http.delete("/api/oglasi/" + id).then(
            function (response) {
              that.dobaviOglase()
            },
            function (reason) {
              console.log(reason)
            }
          )
        }

        //==========================
        this.dobaviKategorije = function () {
          $http.get("/api/kategorije").then(
            function (response) {
              that.kategorije = response.data
            },
            function (reason) {
              console.log(reason)
            }
          )
        }

        this.obrisiKategoriju = function (id) {
          $http.delete("/api/kategorije/" + id).then(
            function (response) {
              that.dobaviKategorije()
            },
            function (reason) {
              console.log(reason)
            }
          )
        }

        that.dobaviKorisnike()
        that.dobaviOglase()
        that.dobaviKategorije()
  
  
      }
    ])
  })(angular)
