import flask
import datetime
from flask import Flask


from utils.db import mysql


from blueprints.korisnici.korisnici import korisnici_blueprint
from blueprints.filmovi.filmovi import filmovi_blueprint
from blueprints.karte.karte import karte_blueprint
from blueprints.prjekcije.projekcije import projekcije_blueprint


app = Flask(__name__, static_url_path="")


app.config["MYSQL_DATABASE_USER"] = "root" 
app.config["MYSQL_DATABASE_PASSWORD"] = "root" 
app.config["MYSQL_DATABASE_DB"] = "bioskop" 
app.config["MYSQL_DATABASE_SOCKET"] = None

mysql.init_app(app) 

app.register_blueprint(korisnici_blueprint, url_prefix="/api")
app.register_blueprint(filmovi_blueprint, url_prefix="/api")
app.register_blueprint(karte_blueprint, url_prefix="/api")
app.register_blueprint(projekcije_blueprint, url_prefix="/api")



@app.route("/")
@app.route("/index")
def index_page():
    
    return app.send_static_file("index.html")

if __name__ == "__main__":
    app.run("0.0.0.0", 5000, threaded=True)
