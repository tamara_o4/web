(function (angular) {
    let app = angular
        .module("app")
        .controller("prikaziProjekcijeCtrl", ["$http", 
        function ($http) {
            let that = this
            
            this.dobaviProjekcije = function(){
                $http.get("/api/projekcije").then(
                    function(response){
                        that.projekcije = response.data
                    },
                    function(reason){
                        console.log(reason)
                    }
                )
            }

            this.dobaviFilmove = function(){
                $http.get("/api/filmovi").then(
                    function(response){
                        that.filmovi = response.data
                    },
                    function(reason){
                        console.log(reason)
                    }
                )
            }

            this.obrisiProjekciju = function(id){
                $http.delete("/api/projekcije/"+id).then(
                    function(response){
                        that.dobaviProjekcije()
                    },
                    function(reason){
                        console.log(reason)
                    }
                )
            }


            this.dobaviFilmove()
            this.dobaviProjekcije()
     

    }])
})(angular)