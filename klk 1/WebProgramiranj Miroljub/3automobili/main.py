import flask
from flask import Flask

app = Flask(__name__,static_url_path="")

automobili = [
    {
        "marka":"audi",
        "model":"RS7",
        "godiste":"2019",
        "tablica":"nsXXX",
        "cena":5000,
        "obrisan":False,
        "rezervacije":[
            {
                "pocetak":"2019-10-08",
                "kraj":"2019-11-1"
            }
        ]
    }
]


@app.route("/")
def pocetna():
    return flask.render_template("pocetna.html",automobili=automobili)

@app.route("/brisanje/<int:index>")
def brisanje(index):
    #del automobili[index]
    automobili[index]["obrisan"] = not automobili[index]["obrisan"]
    return flask.redirect("/")

@app.route("/dodaj",methods=["POST"])
def dodaj():
    automobili.append(    {
        "marka":flask.request.form["marka"],
        "model":flask.request.form["model"],
        "godiste":flask.request.form["godiste"],
        "tablica":flask.request.form["tablica"],
        "cena":int(flask.request.form["cena"]),
        "obrisan":False,
        "rezervacije":[]
    })
    return flask.redirect("/")

@app.route("/izmena/<int:index>")
def izmena(index):
    indexAuta = index
    izabran = automobili[index]
    return flask.render_template("izmena.html",izabran=izabran,index=index)

@app.route("/izmeni/<int:index>",methods=["POST"])
def rldAutomobili(index):
    automobili[index] = {
        "marka":flask.request.form["marka"],
        "model":flask.request.form["model"],
        "godiste":flask.request.form["godiste"],
        "tablica":flask.request.form["tablica"],
        "cena":int(flask.request.form["cena"]),
        "obrisan":automobili[index]["obrisan"],
        "rezervacije":automobili[index]["rezervacije"]
    }
    return flask.redirect("/")

@app.route("/detalji/<tablica>")
def detalji(tablica):
    for a in automobili:
        if a["tablica"] == tablica:
            izabran = a
    return flask.render_template("detalji.html",izabran=a)
    
@app.route("/rezervisan/<tablica>", methods=["POST"])
def rezervisan(tablica):
    for a in automobili:
        if a["tablica"] == tablica:
            a["rezervacije"].append({
                "pocetak":flask.request.form["pocetak"],
                "kraj":flask.request.form["kraj"]
            })
            break
    return flask.redirect("/detalji/<{{tablica}}>")






if __name__ == "__main__":
    app.run("0.0.0.0", 5000, threaded=True)