(function (angular) {
    let app = angular.module("app").controller("izmeniKorisnikaCtrl", [
      "$http",
      "$state",
      "$stateParams",
      function ($http, $state, $stateParams) {
        let that = this;
  
        let noviKorisnik = {
          ime: "",
          prezime: ""
        };
  
        this.dobaviKorisnike = function () {
          $http.get("/api/korisnici").then(
            function (response) {
              that.korisnici = response.data;
            },
            function (reason) {
              console.log(reason);
            }
          );
        };
  
        this.dobaviKorisnika = function (id) {
          $http.get("/api/korisnici/" + id).then(
            function (response) {
              that.noviKorisnik = response.data;
              console.log(response.data);
            },
            function (reason) {
              console.log(reason);
            }
          );
        };
  
        this.izmeniKorisnika = function (id) {
          $http.put("/api/korisnici/" + id, that.noviKorisnik).then(
            function (response) {
              $state.go("home");
            },
            function (reason) {
              console.log(reason);
            }
          );
        };
  
        this.wrapperFunction = function () {
          that.izmeniKorisnika($stateParams["id"]);
        };
  
        that.dobaviKorisnika($stateParams["id"]);
        this.dobaviKorisnike();
      },
    ]);
  })(angular);
  