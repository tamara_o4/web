import flask
from flask import Flask
from datetime import date

app = Flask(__name__,static_url_path="")

korisnici = [
    {   
        "isbn":0,
        "ime":"Miroljub",
        "prezime":"Brkic",
        "username":"miroljubbr",
        "email":"mikibrkic75@gmail.coms"
    }
]

racuni = [
    {
        "id":0,
        "vlasnik":"miroljubbr",
        "balans":3000
    },
    {
        "id":1,
        "vlasnik":"miroljubbr",
        "balans":1000
    }
]

uplate = [
    {
        "posiljalac":0,
        "primalac":1,
        "kolicina":1000,
        "datum":"16/9/2019"
    }
]

@app.route("/")
def pocetna():
    return flask.render_template("pocetna.html", korisnici=korisnici)

@app.route("/obrisi/<int:isbn>")
def brisanje(isbn):
    del korisnici[isbn]
    return flask.redirect("/")

@app.route("/korisnikForma")
def add_forma():
    return flask.render_template("addKorisnika.html");

@app.route("/dodavanje",methods=["POST"])
def dodavanje():
    korisnici.append({
        "isbn":len(korisnici),
        "ime":flask.request.form["ime"],
        "prezime":flask.request.form["prezime"],
        "username":flask.request.form["username"],
        "email":flask.request.form["email"]
    })
    return flask.redirect("/")

@app.route("/detalji/<int:isbn>")
def detalji(isbn):
    racuni_korisnika=[]
    for r in racuni:
        if r["vlasnik"] == korisnici[isbn]["username"]:
            racuni_korisnika.append(r)
    return flask.render_template("detalji.html",izabran=korisnici[isbn],racuni_korisnika=racuni_korisnika);

@app.route("/racunForma/<int:isbn>")
def racunForma(isbn):
    izabran = korisnici[isbn]
    return flask.render_template("addRacun.html",izabran=izabran)

@app.route("/dodavanjeRacuna/<int:isbn>",methods=["POST"])
def dodavanjeRacuna(isbn):
    racuni.append(
        {
            "id":len(racuni),
            "vlasnik":korisnici[isbn]["username"],
            "balans":0
        }
    )
    racuni_korisnika=[]
    for r in racuni:
        if r["vlasnik"] == korisnici[isbn]["username"]:
            racuni_korisnika.append(r)
    return flask.render_template("detalji.html",izabran=korisnici[isbn],racuni_korisnika=racuni_korisnika);

@app.route("/ukloniRacun/<int:isbn>/<int:index>")
def ukloni_racun(isbn,index):
    del racuni[index]
    return flask.render_template("detalji.html",izabran=korisnici[isbn])

@app.route("/uplataForma")
def uplata_forma():
    return flask.render_template("uplata.html",racuni=racuni,uplate=uplate)

@app.route("/uplati",methods=["POST"])
def uplati():
    posiljalac = int(flask.request.form["posiljalac"])
    primalac = int(flask.request.form["primalac"])
    kolicina = int(flask.request.form["kolicina"])

    if racuni[posiljalac]["balans"] < kolicina:
        uplata = False
        return flask.render_template("uplata.html",racuni=racuni, uplate=uplate,uplata = uplata)
    else:
        racuni[posiljalac]["balans"] = racuni[posiljalac]["balans"] - kolicina
        racuni[primalac]["balans"] = racuni[primalac]["balans"] + kolicina
        uplate.append(
            {
                "posiljalac":posiljalac,
                "primalac":primalac,
                "kolicina":kolicina,
                "datum":date.today().strftime("%d/%m/%y")
            }
        )
        return flask.redirect("/uplataForma")




if __name__ == "__main__":
    app.run("0.0.0.0",5000,threaded=True, debug=True)