(function (angular) {
    let app = angular.module("app").controller("dodajProjekcijuCtrl", ["$http","$state",
    function ($http, $state) {
            let that = this

            let novaProjekcija = {
                "film_id":"",
                "termin":"",
                "cena":""
            }

            this.srediDatum = function (callback) {
                that.novaProjekcija.termin.setDate(that.novaProjekcija.termin.getDate() + 1)
                mysqlDateFormat = that.novaProjekcija.termin.toISOString().slice(0, 19)
                that.novaProjekcija.termin = mysqlDateFormat
                callback();
            }

            this.dobaviProjekcije = function(){
                $http.get("/api/projekcije").then(
                    function(response){
                        that.projekcije = response.data
                    },
                    function(reason){
                        console.log(reason)
                    }
                )
            }

            this.dobaviFilmove = function(){
                $http.get("/api/filmovi").then(
                    function(response){
                        that.filmovi = response.data
                    },
                    function(reason){
                        console.log(reason)
                    }
                )
            }

            this.dodajProjekciju = function () {
                $http.post("/api/projekcije", that.novaProjekcija).then(
                    function (response) {
                        $state.go("prikaziProjekcije");
                    },
                    function (reason) {
                        console.log(reason);
                    }
                )
            }

            this.wrapperFunction = function () {
                that.srediDatum(that.dodajProjekciju)
            }

            this.dobaviFilmove()
            
        }
    ])
})(angular)