import flask
import datetime
from flask import Flask

from utils.db import mysql

# Dobavljanje blueprint-ova.
"""from blueprints.kategorije import kategorije_blueprint"""
from blueprints.korisnici.korisnici import korisnici_blueprint
from blueprints.oglasi.oglasi import oglasi_blueprint
from blueprints.kategorije.kategorije import kategorije_blueprint


app = Flask(__name__, static_url_path="")

app.config["MYSQL_DATABASE_USER"] = "root" 
app.config["MYSQL_DATABASE_PASSWORD"] = "root" 
app.config["MYSQL_DATABASE_DB"] = "oglasi"
app.config["SECRET_KEY"] = "ta WJoir29$"

mysql.init_app(app) 
# Registrovanje blueprint-a.
"""app.register_blueprint(kategorije_blueprint, url_prefix="/api")"""
app.register_blueprint(korisnici_blueprint, url_prefix="/api")
app.register_blueprint(oglasi_blueprint, url_prefix="/api")

app.register_blueprint(kategorije_blueprint, url_prefix="/api")



@app.route("/")
@app.route("/index")
def index_page():
    return app.send_static_file("index.html")

if __name__ == "__main__":
    app.run("0.0.0.0", 5000, threaded=True)

# Nakon pokretanja aplikacije u web browser je moguce
# uneti URL http://localhost:5000 i videti rezultat ove skripte.
