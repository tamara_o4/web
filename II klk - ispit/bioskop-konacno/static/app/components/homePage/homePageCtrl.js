(function (angular) {
    let app = angular
        .module("app")
        .controller("homePageCtrl", ["$http", 
        function ($http) {
            let that = this
            
            this.dobaviKorisnike = function(){
                $http.get("/api/korisnici").then(
                    function(response){
                        that.korisnici = response.data
                    },
                    function(reason){
                        console.log(reason)
                    }
                )
            }

            this.dobaviFilmove = function(){
                $http.get("/api/filmovi").then(
                    function(response){
                        that.filmovi = response.data
                    },
                    function(reason){
                        console.log(reason)
                    }
                )
            }

            this.obrisiKorisnika = function(id){
                $http.delete("/api/korisnici/"+id).then(
                    function(response){
                        that.dobaviKorisnike()
                    },
                    function(reason){
                        console.log(reason)
                    }
                )
            }

            this.obrisiFilm = function(id){
                $http.delete("/api/filmovi/"+id).then(
                    function(response){
                        that.dobaviFilmove()
                    },
                    function(reason){
                        console.log(reason)
                    }
                )
            }

            this.dobaviKorisnike()
            this.dobaviFilmove()
     

    }])
})(angular)