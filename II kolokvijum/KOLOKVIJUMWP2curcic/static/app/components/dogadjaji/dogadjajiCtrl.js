(function (angular) {
    var app = angular.module("app");
    app.controller("dogadjajCtrl", ["$http", "$state", "$stateParams", function ($http, $state, $stateParams) {
        var that = this;                                
                                    //Na zalost zbog nedostatka vremena nisam stigao ostatak da uradim za dogadjaje iako ih znam
                                    // Sto se tice dodavanje novih dogadjaja planirao sam da napravim dogadjajFormu i tamo ih kao i za prethodne primere uradim
                                    //ng-option za selectovanje se isto lako implementira pri pravljenju dogadjaja
        this.tipovi = []; 
        this.noviDogadjaj = {
            "tip_dogadjaja": null,
            "naziv": "",
            "datum": "",
            "cena_karte": 0
        };
        this.dogadjaji = [];

        this.dobaviDogadjaje= function() {

            $http.get("api/dogadjaj").then(function(result){

                console.log(result);

                that.dogadjaji = result.data;
    
                 

            },
            function(reason) {
                console.log(reason);
            });
        }
        this.dobaviTipove = function() {
            $http.get("api/tipovi").then(function(result){
                console.log(result);
                that.tipovi = result.data;
                if(!$stateParams["id"]) {
                    that.noviDogadjaj.tip_dogadjaja = that.tipovi[0].id;
                }
                 

            },
            function(reason) {
                console.log(reason);
            });
        }

        this.dobaviDogadjaj= function(id) {
            $http.get("api/dogadjaj/" + id).then(function(result){
                that.noviDogadjaj = result.data;
                that.noviDogadjaj["tip_dogadjaja"] = that.noviDogadjaj["tip_dogadjaja_id"];
            }, function(reason) {
                console.log(reason);
            });
        }
        this.ukloniDogadjaj = function(id) {
            
            $http.delete("api/dogadjaj/" + id).then(function(response){
                console.log(response);
                that.dobaviDogadjaje();
            },
            function(reason){
                console.log(reason);
                
            });
        }

        this.dodajDogadjaj = function () {
            
            $http.post("api/dogadjaji", this.noviDogadjaj).then(function (response) {
                console.log(response);
                
                $state.go("home")
            }, function (reason) {
                console.log(reason);
               
            });
        }
        this.izmeniDogadjaj = function(id) {
            $http.put("api/dogadjaj/" + id, that.noviDogadjaj).then(function(response) {
                console.log(response)
               
            }, function(reason) {
                console.log(reason);
                
            });
        }

        this.sacuvaj = function() {
            if($stateParams["id"]){
            this.izmeniDogadjaj($stateParams["id"], that.dogadjaj);
            }else{
                this.dodajDogadjaj()
            }
            
        }

        if($stateParams["id"]) {
            this.dobaviDogadjaj($stateParams["id"]);
        }

        this.dobaviTipove();
        if($stateParams["id"]) {
            this.dobaviDogadjaj($stateParams["id"]);
        }
        this.dobaviDogadjaje();
    }]);
})(angular);