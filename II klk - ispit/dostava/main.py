import flask
import datetime
from flask import Flask

# Dobavljanje mysql promenljive iz db modula.
from utils.db import mysql

# Dobavljanje blueprint-ova.
"""from blueprints.kategorije import kategorije_blueprint"""
from blueprints.korisnici.korisnici import korisnici_blueprint


app = Flask(__name__, static_url_path="")

#Podesavanje konfiguracije za povezivanje na bazu podataka.
app.config["MYSQL_DATABASE_USER"] = "root" # Korisnicko ime.
app.config["MYSQL_DATABASE_PASSWORD"] = "root" # Lozinka.
app.config["MYSQL_DATABASE_DB"] = "dostava" # Naziv seme baze podataka na koju se treba povezati
app.config["SECRET_KEY"] = "ta WJoir29$" # Podesavanje tajnog kljuca koji se koristi za enkripciju kolacica.
                                         # Ovaj string je pozeljno izgenerisati nasumicno pri pokretanju
                                         # aplikacije.

mysql.init_app(app) # Naknadno prosledjivanje instance aplikacije
                    # za konfigurisanje objekta za upravljanje
                    # konekcijama ka bazi podataka.

# Registrovanje blueprint-a.
"""app.register_blueprint(kategorije_blueprint, url_prefix="/api")"""
app.register_blueprint(korisnici_blueprint, url_prefix="/api")


@app.route("/")
@app.route("/index")
def index_page():
    return app.send_static_file("index.html")

if __name__ == "__main__":
    app.run("0.0.0.0", 5000, threaded=True)

# Nakon pokretanja aplikacije u web browser je moguce
# uneti URL http://localhost:5000 i videti rezultat ove skripte.
