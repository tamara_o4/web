import flask
from flask.blueprints import Blueprint

from utils.db import mysql

korisnici_blueprint = Blueprint("korisnici_blueprint", __name__)

@korisnici_blueprint.route("/korisnici", methods=["GET"])
def dobavi_korisnike():
    cursor = mysql.get_db().cursor() 
    cursor.execute("SELECT * FROM korisnik") 
    korisnici = cursor.fetchall() 
    return flask.jsonify(korisnici) 


@korisnici_blueprint.route("/korisnici/<int:id_korisnika>")
def dobavi_korisnika(id_korisnika, methods=["GET"]):
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM korisnik WHERE id=%s", (id_korisnika,))
    proizvod = cursor.fetchone()
    if proizvod is not None:
        return flask.jsonify(proizvod)
    else:
        print("DOSLO JE DO GRESKE PRILIKOM DOBAVLJANJA KORISNIKA")
        return "", 404
@korisnici_blueprint.route("/korisnici", methods=["POST"])
def dodaj_korisnika():
    db = mysql.get_db() 
    cursor = db.cursor()
    print(flask.request.json)
    cursor.execute("INSERT INTO korisnik( ime, prezime) VALUES( %(ime)s, %(prezime)s)", flask.request.json)
    db.commit()
    return flask.jsonify(flask.request.json), 201 

@korisnici_blueprint.route("/korisnici/<int:id_korisnika>", methods=["PUT"])
def izmeni_korisnika(id_korisnika):
   
    db = mysql.get_db()
    cursor = db.cursor()
    data = flask.request.json
    data["id"] = id_korisnika 
    cursor.execute("UPDATE korisnik SET prezime=%(prezime)s, ime=%(ime)s WHERE id=%(id)s", data)
    db.commit()
    return "", 200


@korisnici_blueprint.route("/korisnici/<int:id_korisnika>", methods=["DELETE"])
def ukloni_korisnika(id_korisnika):
    print(id_korisnika)
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("DELETE FROM korisnik WHERE id=%s", (id_korisnika,))
    db.commit()
    return "", 204 
