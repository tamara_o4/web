import flask
from flask.blueprints import Blueprint

from utils.db import mysql

oglasi_blueprint = Blueprint("oglasi_blueprint", __name__)

@oglasi_blueprint.route("/oglasi", methods=["GET"])
def dobavi_oglase():
    cursor = mysql.get_db().cursor() 
    cursor.execute("SELECT * FROM oglas") 
    oglasi = cursor.fetchall() 
    return flask.jsonify(oglasi) 

@oglasi_blueprint.route("/oglasi/<int:id_oglasa>")
def dobavi_oglas(id_oglasa, methods=["GET"]):
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM oglas WHERE id=%s", (id_oglasa,))
    oglas = cursor.fetchone() 
    if oglas is not None:
        return flask.jsonify(oglas) 
    else:
        print("DOSLO JE DO GRESKE PRILIKOM DOBAVLJANJA KORISNIKA")
        return "", 404 

@oglasi_blueprint.route("/oglasi", methods=["POST"])
def dodaj_oglas():
    db = mysql.get_db() 
    cursor = db.cursor()    
    print(flask.request.json)
    cursor.execute("INSERT INTO oglas(kategorija_id, korisnik_id, naslov, sadrzaj, datum_objave) VALUES(%(kategorija_id)s, %(korisnik_id)s, %(naslov)s, %(sadrzaj)s, %(datum_objave)s)", flask.request.json)
  
    db.commit()
    return flask.jsonify(flask.request.json), 201 

@oglasi_blueprint.route("/oglasi/<int:id_oglasa>", methods=["PUT"])
def izmeni_korisnika(id_oglasa):
    db = mysql.get_db()
    cursor = db.cursor()
    data = flask.request.json
    data["id"] = id_oglasa 
    cursor.execute("UPDATE oglas SET kategorija_id=%(kategorija_id)s, korisnik_id=%(korisnik_id)s, naslov=%(naslov)s, sadrzaj=%(sadrzaj)s, datum_objave=%(datum_objave)s  WHERE id=%(id)s", data)
    db.commit()
    return "", 200


@oglasi_blueprint.route("/oglasi/<int:id_oglasa>", methods=["DELETE"])
def ukloni_oglas(id_oglasa):
    print(id_oglasa)
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("DELETE FROM oglas WHERE id=%s", (id_oglasa,))
    db.commit()
    return "", 204 
