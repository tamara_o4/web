(function (angular) {
    let app = angular.module("app").controller("dodajKorisnikaCtrl", [
      "$http",
      "$state",
      function ($http, $state) {
        let that = this
  
        let noviKorisnik = {
          "username": "",
          "ime":"",
          "prezime":"",
        }

  
        this.dodajKorisnika = function () {
          $http.post("/api/korisnici", that.noviKorisnik).then(
            function (response) {
              $state.go("home")
            },
            function (reason) {
              console.log(reason)
            }
          )
        }
        this.wrapperFunction = function () {
          that.dodajKorisnika();
        }

      }
    ])
  })(angular)
  