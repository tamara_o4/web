import flask
from flask import Flask
from flask import request

app = Flask(__name__, static_url_path="")

dobavljaci = [
    {
        "adresa": {
            "ulica": "BcL 71",
            "grad": "Novi Sad",
            "drzava": "Srbija"
        },
        "naziv": "Mobilni shop",
        "obrisan": False
    }
]
stavke = [
    {
        "naziv": "iphone X",
        "dobavljac": "Mobilni shop",
        "jm": "komad"
    }
]
stavke_na_stanju = [
    {
        "stavka":"iphone X",
        "kolicina":10
    }
]

#putanja koja prikazuje pocetnu stranu sa tabelom dobavljaca
@app.route("/")
def tabela_dobavljaca():
    return flask.render_template("tabelaDobavljaca.html", dobavljaci=dobavljaci)

#putanja na koju ide kada brise dobavljaca nakon cega ide na pocetnu
@app.route("/brisanje/<int:index>")
def brisanje(index):
    dobavljaci[index]["obrisan"] = not dobavljaci[index]["obrisan"]
    return flask.redirect("/")

#putanja ka formi za dodavanje dobavljaca
@app.route("/dodavanjeDobavljaca")
def dodavanje_dobavljaca():
    return flask.render_template("addDobavljaca.html")

#putanja koja prikazuje nakon dodavanja tabelu dobavljaca
@app.route("/izmenjenPrikaz", methods=["post"])
def izmenjen_prikaz():
    dobavljaci.append({
        "adresa": {
            "ulica": flask.request.form["ulica"],
            "grad": flask.request.form["grad"],
            "drzava": flask.request.form["drzava"]
        },
        "naziv": flask.request.form["naziv"],
        "obrisan": False
    })
    return flask.redirect("/")

#putanja ka formi za dodavanje stavke i tabeli svih stavki
@app.route("/dodavanjeStavki")
def dodavanje_stavke():
    return flask.render_template("addStavku.html", stavke=stavke,dobavljaci=dobavljaci)

#putanja za dodavanje stavke i ponovnog ucitavanja iste strane
@app.route("/izmenjenPrikzaStavki",methods=["POST"])
def izmenjen_prikaz_stavki():
    stavke.append({
        "naziv": flask.request.form["naziv"],
        "dobavljac": flask.request.form["dobavljac"],
        "jm": flask.request.form["jm"]
    })
    return flask.redirect("/dodavanjeStavki")

#putanja ka formi za dodavanje stavki na stanju i tabeli svih stavki
@app.route("/dodavanjeStavkiNaStanju")
def dodavanje_stavki_na_stanju():
    return flask.render_template("addStavkuNaStanju.html",stavke_na_stanju = stavke_na_stanju,stavke=stavke)


#putanja za dodavanje stavke i ponovnog ucitavanja iste strane
@app.route("/izmenjenPrikzaStavkiNaStanju",methods=["POST"])
def izmenjen_prikaz_stavki_na_stanju():
    postoji = False
    for stavka in stavke_na_stanju:
        if stavka["stavka"] == flask.request.form["stavka"]:
            postoji = True
            stavka["kolicina"] = stavka["kolicina"] + int(flask.request.form["kolicina"])
    if postoji == False:
        stavke_na_stanju.append({
            "stavka":flask.request.form["stavka"],
            "kolicina":int(flask.request.form["kolicina"])
        })
    return flask.redirect("/dodavanjeStavkiNaStanju")

@app.route("/statistikaDobavljaca")
def statistika():
    return flask.render_template("statistika.html",stavke=stavke,dobavljaci=dobavljaci)



if __name__ == "__main__":
    app.debug = True
    app.run("0.0.0.0", 5000, threaded=True)
