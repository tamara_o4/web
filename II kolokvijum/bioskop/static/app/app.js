(function(angular){
    // Kreiranje modula pod nazivom app koji kao zavisnost ima ui router modul.
    // U slucaju da je modul vec kreiran, umesto kreiranja novog, bice
    // dobavljen postojeci modul.
    var app = angular.module("app", ["ui.router"]);

    app.config(["$stateProvider", "$urlRouterProvider", function($stateProvider, $urlRouterProvider) {
        // Stanje se registruje preko $stateProvider servisa, pozivom
        // metode state kojoj se prosledjuje state objekat.
        $stateProvider.state({
            name: "home", // Naziv stanja, mora biti zadat.
            url: "/", // URL kojim se prelazi na zadato stanje.
            templateUrl: "app/components/korisnici/korisnici.tpl.html", // URL do sablona koji se koristi za zadato stanje.
            controller: "KorisniciCtrl", // Kontroler koji se vezuje za sablon u zadatom stanju.
            controllerAs: "pctrl" // Naziv vezanog kontrolera.
        }).state({
            name: "dodajKorisnika",
            url: "/korisnikForma",
            templateUrl: "app/components/korisnikForma/korisnikForma.tpl.html",
            controller: "KorisnikFormaCtrl",
            controllerAs: "pctrl"
        }).state({
            name: "korisnici",
            url: "/korisnici",
            templateUrl: "app/components/korisnici/korisnici.tpl.html",
            controller: "KorisniciCtrl",
            controllerAs: "pctrl"
        })/*.state({
            name: "proizvod",
            url: "/proizvodi/{id}", // Ovaj URL je razlicit od URL-a za pristup svim proizvodima
                                    // jer sadrzi parametar id. Samo ukoliko se navede i parametar
                                    // dolazi do poklapanja sa ovim URL-om i do aktiviranja stanja.
            templateUrl: "app/components/proizvod/proizvod.tpl.html",
            controller: "ProizvodCtrl",
            controllerAs: "pctrl"
        })*/.state({
            name: "izmeniKorisnika",
            url: "/korisnikForma/{id}",
            templateUrl: "app/components/korisnikForma/korisnikForma.tpl.html",
            controller: "korisnikFormaCtrl", // Isti kontroler se moze koristiti vise puta.
            controllerAs: "pctrl"
        })/*.state({
            name: "login",
            url: "/login",
            templateUrl: "app/components/login/login.tpl.html",
            controller: "loginCtrl", // Isti kontroler se moze koristiti vise puta.
            controllerAs: "lctrl"
        })*/;

        // Ukoliko zadati url ne pokazuje ni na jedno stanje
        // vrsi se preusmeravajne na url zadat pozivom metode
        // otherwise nad $urlRouterProvider servisom.
        $urlRouterProvider.otherwise("/")
    }])
})(angular);