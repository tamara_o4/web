(function(angular){
   
    let app = angular.module("app", ["ui.router"]);

    app.config(["$stateProvider", "$urlRouterProvider", function($stateProvider, $urlRouterProvider) {

        $stateProvider.state({
            name: "home",
            url: "/", 
            templateUrl: "app/components/homePage/homePage.tpl.html", 
            controller: "homePageCtrl", 
            controllerAs: "ctrl" 
        })
        .state({
            name: "dodajKorisnika",
            url: "/dodajKorisnika", 
            templateUrl: "app/components/dodajKorisnika/dodajKorisnika.tpl.html", 
            controller: "dodajKorisnikaCtrl", 
            controllerAs: "ctrl" 
        })
        .state({
            name: "dodajFilm",
            url: "/dodajFilm", 
            templateUrl: "app/components/dodajFilm/dodajFilm.tpl.html", 
            controller: "dodajFilmCtrl", 
            controllerAs: "ctrl" 
        })
        .state({
            name: "izmeniFilm",
            url: "/izmeniFilm/{id}", 
            templateUrl: "app/components/izmeniFilm/izmeniFilm.tpl.html", 
            controller: "izmeniFilmCtrl", 
            controllerAs: "ctrl" 
        })
        .state({
            name: "izmeniKorisnika",
            url: "/izmeniKorisnika/{id}", 
            templateUrl: "app/components/izmeniKorisnika/izmeniKorisnika.tpl.html", 
            controller: "izmeniKorisnikaCtrl", 
            controllerAs: "ctrl" 
        })
        .state({
            name: "prikaziProjekcije",
            url: "/prikaziProjekcije", 
            templateUrl: "app/components/prikaziProjekcije/prikaziProjekcije.tpl.html", 
            controller: "prikaziProjekcijeCtrl", 
            controllerAs: "ctrl" 
        })
        .state({
            name: "dodajProjekciju",
            url: "/dodajProjekciju", 
            templateUrl: "app/components/dodajProjekciju/dodajProjekciju.tpl.html", 
            controller: "dodajProjekcijuCtrl", 
            controllerAs: "ctrl" 
        })
        .state({
            name: "izmeniProjekciju",
            url: "/izmeniProjekciju/{id}", 
            templateUrl: "app/components/izmeniProjekciju/izmeniProjekciju.tpl.html", 
            controller: "izmeniProjekcijuCtrl", 
            controllerAs: "ctrl" 
        })
        .state({
            name: "prikaziKarte",
            url: "/prikaziKarte", 
            templateUrl: "app/components/prikaziKarte/prikaziKarte.tpl.html", 
            controller: "prikaziKarteCtrl", 
            controllerAs: "ctrl" 
        })
        .state({
            name: "dodajKartu",
            url: "/dodajKartu", 
            templateUrl: "app/components/dodajKartu/dodajKartu.tpl.html", 
            controller: "dodajKartuCtrl", 
            controllerAs: "ctrl" 
        })
        
        
        $urlRouterProvider.otherwise("/")
    }])
})(angular);