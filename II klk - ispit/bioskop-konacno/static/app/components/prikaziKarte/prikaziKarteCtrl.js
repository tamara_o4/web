(function (angular) {
    let app = angular
        .module("app")
        .controller("prikaziKarteCtrl", ["$http",
        function ($http) {
            let that = this
            
            this.dobaviKarte = function(){
                $http.get("/api/karte").then(
                    function(response){
                        that.karte = response.data
                    },
                    function(reason){
                        console.log(reason)
                    }
                )
            }

            this.dobaviKarte()
     

    }])
})(angular)