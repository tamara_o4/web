import flask
from flask.blueprints import Blueprint

from utils.db import mysql

filmovi_blueprint = Blueprint("filmovi_blueprint", __name__)

@filmovi_blueprint.route("/filmovi", methods=["GET"])
def dobavi_korisnike():
    cursor = mysql.get_db().cursor() 
    cursor.execute("SELECT * FROM film") 
    filmovi = cursor.fetchall() 
    return flask.jsonify(filmovi) 

@filmovi_blueprint.route("/filmovi/<int:id_filma>")
def dobavi_korisnika(id_filma, methods=["GET"]):
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM film WHERE id=%s", (id_filma,))
    proizvod = cursor.fetchone() 
    if proizvod is not None:
        return flask.jsonify(proizvod) 
    else:
        print("DOSLO JE DO GRESKE PRILIKOM DOBAVLJANJA KORISNIKA")
        return "", 404 

@filmovi_blueprint.route("/filmovi", methods=["POST"])
def dodaj_korisnika():
    db = mysql.get_db() 
    cursor = db.cursor() 

    print(flask.request.json)
    cursor.execute("INSERT INTO film(naziv, zanr, reziser,premijera) VALUES(%(naziv)s, %(zanr)s, %(reziser)s, %(premijera)s)", flask.request.json)
  
    db.commit()
    return flask.jsonify(flask.request.json), 201 

@filmovi_blueprint.route("/filmovi/<int:id_filma>", methods=["PUT"])
def izmeni_korisnika(id_filma):
   
    db = mysql.get_db()
    cursor = db.cursor()
    data = flask.request.json
    data["id"] = id_filma 
    cursor.execute("UPDATE film SET naziv=%(naziv)s, zanr=%(zanr)s, reziser=%(reziser)s, premijera=%(premijera)s WHERE id=%(id)s", data)
    db.commit()
    return "", 200


@filmovi_blueprint.route("/filmovi/<int:id_filma>", methods=["DELETE"])
def ukloni_korisnika(id_filma):
    print(id_filma)
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("DELETE FROM film WHERE id=%s", (id_filma,))
    db.commit()
    return "", 204 
