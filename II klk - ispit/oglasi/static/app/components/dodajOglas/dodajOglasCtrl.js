(function (angular) {
    let app = angular.module("app").controller("dodajOglasCtrl", [
      "$http",
      "$state",
      function ($http, $state) {
        let that = this
  
        let noviOglas = {
          "kategorija_id": "",
          "korisnik_id":"",
          "naslov":"",
          "sadrzaj":"",
          "datum_objave":"",
        }


        this.srediDatum = function (callback,param) {
          //mysqlDateFormat = that.novaProjekcija.termin.toISOString().slice(0, 10);
          //that.novaProjekcija.termin = mysqlDateFormat;
          that.noviOglas.datum_objave.setDate(that.noviOglas.datum_objave.getDate() + 1)
          mysqlDateFormat = that.noviOglas.datum_objave.toISOString().slice(0, 19)
          that.noviOglas.datum_objave = mysqlDateFormat
          callback(param);
        }

        this.dobaviKorisnike = function () {
            $http.get("/api/korisnici").then(
              function (response) {
                that.korisnici = response.data
              },
              function (reason) {
                console.log(reason)
              }
            )
          }

          this.dobaviKategorije = function () {
            $http.get("/api/kategorije").then(
              function (response) {
                that.kategorije = response.data
              },
              function (reason) {
                console.log(reason)
              }
            )
          }
  
        this.dodajOglas = function () {
          $http.post("/api/oglasi", that.noviOglas).then(
            function (response) {
              $state.go("home")
            },
            function (reason) {
              console.log(reason)
            }
          )
        }

        this.dobaviKorisnike();
        this.dobaviKategorije();
        this.wrapperFunction = function () {
          that.srediDatum(that.dodajOglas)
        }

      }
    ])
  })(angular)
  