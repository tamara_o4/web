(function (angular) {
    let app = angular.module("app").controller("izmeniProjekcijuCtrl", ["$http","$state","$stateParams",
    function ($http, $state,$stateParams) {
            let that = this

            let novaProjekcija = {
                "film_id":"",
                "termin":"",
                "cena":""
            }

            this.srediDatum = function (callback,param) {
                //mysqlDateFormat = that.novaProjekcija.termin.toISOString().slice(0, 10);
                //that.novaProjekcija.termin = mysqlDateFormat;
                that.novaProjekcija.termin.setDate(that.novaProjekcija.termin.getDate() + 1)
                mysqlDateFormat = that.novaProjekcija.termin.toISOString().slice(0, 19)
                that.novaProjekcija.termin = mysqlDateFormat
                callback(param);
            }

            this.dobaviFilmove = function(){
                $http.get("/api/filmovi").then(
                    function(response){
                        that.filmovi = response.data
                    },
                    function(reason){
                        console.log(reason)
                    }
                )
            }

            this.dobaviProjekciju = function(id){
                $http.get("/api/projekcije/"+id).then(
                    function(response){
                        that.novaProjekcija = response.data
                    },
                    function(reason){
                        console.log(reason)
                    }
                )
            }

            this.izmeniProjekciju = function (id) {
                $http.put("/api/projekcije/"+id, that.novaProjekcija).then(
                    function (response) {
                        $state.go("prikaziProjekcije");
                    },
                    function (reason) {
                        console.log(reason);
                    }
                )
            }

            this.wrapperFunction = function () {
                that.srediDatum(that.izmeniProjekciju,$stateParams['id'])
            }

            this.dobaviProjekciju($stateParams['id'])
            this.dobaviFilmove()
            
        }
    ])
})(angular)