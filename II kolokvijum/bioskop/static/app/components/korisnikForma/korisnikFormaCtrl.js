(function (angular) {
    var app = angular.module("app");
    app.controller("KorisniciFormaCtrl", ["$http", "$state", "$stateParams", function ($http, $state, $stateParams) {
        var that = this;

        // Inicijalno podaci o novom korisniku su popunjeni podrazumevanim vrednostima.
        this.noviKorisnik = {
            "korisnicko_ime": "",
            "ime": "",
            "prezime": "",
        };

        this.dobaviKorisnika = function(id) {
            $http.get("api/korisnici/" + id).then(function(result){
                that.noviKorisnik = result.data;
            }, function(reason) {
                console.log(reason);
            });
        }

        // Funkcija za dodavanje proizvoda.
        this.dodajKorisnika = function () {
            // Upucuje se post zahtev na relativni URL api/proizvodi.
            // Uz url funkciji post se prosledjuje i objekat koji
            // treba da se prosledi u telu zahteva. Angular ce automatski
            // ovaj objekat pretvoriti u JSON format i tako ga proslediti
            // u telu zahteva.
            $http.post("api/korisnici", this.noviKorisnik).then(function (response) {
                console.log(response);
                // Pri uspesnom dodavanju proizvoda precice se na stranicu za prikaz
                // svih proizvoda. Prelazak na stranicu moze se postici pozivom metode
                // go iz $state servisa. Ovoj metodi se prosledjuje naziv stanja na
                // koje je potrebno preci i po potrebi objekat koji sadrzi vrednosti
                // parametara koje treba proslediti pri prelasku u navedeno stanje.
                $state.go("korisnici")
            }, function (reason) {
                console.log(reason);
                // Ukoliko neprijavljeni korisnik pokusa da doda proizvod
                // prikazuje se stranica za prijavu.
                /*if(reason.status == 403) {
                    $state.go("login");
                }*/
            });
        }

        this.izmeniKorisnika = function(id) {
            $http.put("api/korisnici/" + id, that.noviKorisnik).then(function(response) {
                console.log(response)
                // Prelazak na stanje sa parametrizovanim ULR-om pri cemu
                // se vrednost parametra prosledjuje prilikom poziva
                // metode go.
                $state.go("korisnik", {"id": id});
            }, function(reason) {
                console.log(reason);
                // Ukoliko neprijavljeni korisnik pokusa da izmeni proizvod
                // prikazuje se stranica za prijavu.
                if(reason.status == 403) {
                    $state.go("login");
                }
            });
        }

        this.sacuvaj = function() {
            if($stateParams["id"]) {
                this.izmeniKorisnika($stateParams["id"], that.korisnik);
            } else {
                this.dodajKorisnika();
            }
        }

       /*
        this.getCurrentUser = function() {
            $http.get("api/currentUser").then(
                function(response) {
                    // Korisnik ne moze pristupiti ovoj stranici ako nije ulogovan,
                    // vec ce biti preusmeren na login stranicu.
                    if(!response.data) {
                        $state.go("login");
                    }
                },
                function(reason) {
                    console.log(reason);
                }
            )
        }
        
        this.getCurrentUser(); // Provera da li je korisnik vec prijavljen.
         */
    }]);
})(angular);