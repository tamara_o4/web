import flask
from flask import   Flask

app = Flask(__name__, static_url_path="")

stanice = [
    {
        "naziv": "Bicikl 1",
        "adresa": "Bulevar Oslobodjenja 7",
        "datum": "12.1.2020.",
        "status": "aktivan",
        "dostupni": 4,
        "obrisan" : False
        
    },
     {
        "naziv": "Bicikl 2",
        "adresa": "Bulevar Cara Lazara 33",
        "datum": "14.1.2020.",
        "status": "neaktivan",
        "dostupni": 6,
        "obrisan" : False
        
    }
]

karte = []

#putanja koja prikazuje pocetnu stranu sa tabelom stanica
@app.route("/")
def tabela_stanica():
    return flask.render_template("tabelaStanica.html", stanice=stanice)

#putanja na koju ide kada brise stanicu nakon cega ide na pocetnu
@app.route("/brisanje/<int:index>")
def brisanje(index):
    stanice[index]["obrisan"] = not stanice[index]["obrisan"]
    return flask.redirect("/")

#putanja ka formi za dodavanje stanice
@app.route("/dodavanjeStanice")
def dodavanje_dobavljaca():
    return flask.render_template("addStanicu.html")

#putanja koja prikazuje nakon dodavanja tabelu stanica
@app.route("/izmenjenPrikaz", methods=["post"])
def izmenjen_prikaz():
    stanice.append({
        "naziv": flask.request.form["naziv"],
        "adresa": flask.request.form["adresa"],
        "datum": flask.request.form["datum"],
        "status": flask.request.form["status"],
        "dostupni": flask.request.form["dostupni"],
        "obrisan": False
    })
    return flask.redirect("/")

#izmena stanice
@app.route("/izmeniFormu/<int:index>")
def izmeniFormu(index):
    return flask.render_template("izmena.html", stanice=stanice[index], index=index)

@app.route("/izmeni/<int:index>", methods=["POST"])
def izmena(index):
    stanice[index]={
        "naziv": flask.request.form["naziv"],
        "adresa": flask.request.form["adresa"],
        "datum": flask.request.form["datum"],
        "status": flask.request.form["status"],
        "dostupni": flask.request.form["dostupni"],
        "obrisan": False
    }
    return flask.redirect("/")

#kupovnina karata
@app.route("/kupiFomra/<int:index>")
def kupiFomra(index):
    return flask.render_template("kupi.html", stanice=stanice[index], index=index)

@app.route("/kupi/<int:index>", methods=["POST"])
def kupi(index):
    karte.append({
        "naziv": stanice[index]["naziv"],
        "kolicina": flask.request.form["kolicina"]
        
    })

if __name__=="__main__":
    app.run("0.0.0.0", 5000, threded=True, debug=True)