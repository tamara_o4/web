(function(angular){
    // Dobavljanje postojeceg modula app.
    var app = angular.module("app");

    // Kreiranje kontrolera pod nazivom ProizvodiCtrl.
    // Ovaj kontroler zavisi od servisa $http. Zavisnosti
    // se navode kao spisak stringova koji sadrze nazive
    // zavisnosti. Umetanje zavisnosti vrsi se preko parametara
    // funkcije koja predstavlja implementaciju kontrolera.
    // Nazivi parametara mogu biti proizvoljni.
    // Razlog za ovakav nacin umetanja zavisnosti je sto se
    // prilikom minifikacije stringovi ne menjaju pa ce se
    // samim tim sacuvati nazivi zavisnosti. Da su zavisnosti
    // navedene samo kao parametri funkcije, tokom minifikacije,
    // nazivi parametara bi se promenili pa nazivi navedenih
    // zavisnosti ne bi odgovarali nazivima zadatih zavisnosti.
    app.controller("KorisniciCtrl", ["$http" , "$state", function($http, $state) {
        var that = this; // Neophodno je primenovati this
                         // kako bi se promenljiva this mogla
                         // koristiti u ugnjezdenim funkcijama.

        this.korisnici = []; // Inicijalno proizvodi nisu dobavljeni.
        
        // Funkcija za dobavljanje proizvoda.
        this.dobaviKorisnike = function() {
            // Upucuje se get zahtev na relativni URL api/proizvodi.
            $http.get("api/korisnici").then(function(result){
                console.log(result);
                that.korisnici = result.data;
            },
            function(reason) {
                console.log(reason);
            });
        }

        // Funkcija za uklanjanje proizvoda.
 /*       this.ukloniKorisnika = function(id) {
            // Pri uklanjanju proizvoda serveru se salje delete zahtev
            // na url api/proizvodi/<id> pri cemu id zavisi od proizvoda
            // koji je neophodno obrisati.
            $http.delete("api/korisnici/" + id).then(function(response){
                console.log(response);
                that.dobaviKorisnike();
            },
            /*function(reason){
                console.log(reason);
                // Ukoliko neprijavljeni korisnik pokusa da ukloni proizvod
                // prikazuje se stranica za prijavu.
                if(reason.status == 403) {
                    $state.go("login");
                }
            });
        
        }
*/ 
        this.dobaviKorisnike();
    }]);
})(angular);