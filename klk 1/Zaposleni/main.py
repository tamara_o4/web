import flask
from flask import Flask
from flask import request

app = Flask(__name__, static_url_path="")

zaposleni = [
    {
        "ime": "Petar",
        "prezime": "Peric",
        "datum": "2.3.2019.",
        "plata": "30000",
        "rmesto": "pekar",
        "obrisan": False
    },
    {
        "ime": "Sasa",
        "prezime": "Savic",
        "datum": "2.3.2019.",
        "plata": "30000",
        "rmesto": "pekar",
        "obrisan": False
    }
]

angazovanja = []

#putanja koja prikazuje pocetnu stranu sa tabelom zaposlenih
@app.route("/")
def tabela_zaposlenih():
    return flask.render_template("tabelaZaposlenih.html", zaposleni=zaposleni)

#putanja na koju ide kada brise zaposlenog nakon cega ide na pocetnu
@app.route("/brisanje/<int:index>")
def brisanje(index):
    zaposleni[index]["obrisan"] = not zaposleni[index]["obrisan"]
    return flask.redirect("/")

#putanja ka formi za dodavanje zaposlenog
@app.route("/dodavanjeZaposlenog")
def dodavanje_zaposlenog():
    return flask.render_template("addZaposlenog.html")


#putanja koja prikazuje nakon dodavanja tabelu zaposlenog
@app.route("/izmenjenPrikaz", methods=["post"])
def izmenjen_prikaz():
    zaposleni.append({
        "ime": flask.request.form["ime"],
        "prezime": flask.request.form["prezime"],
        "datum": flask.request.form["datum"],
        "plata": flask.request.form["plata"],
        "rmesto": flask.request.form["rmesto"],
        "obrisan": False
    })
    return flask.redirect("/")

#putanja ka formi za izmenu zaposlenog
@app.route("/izmeniFormu/<int:index>")
def izmeniFormu(index):
    return flask.render_template("izmena.html", zaposleni=zaposleni[index], index=index)

#putanja koja prikazuje nakon izmete tabelu zaposlenih
@app.route("/izmeni/<int:index>", methods=["POST"])
def izmena(index):
    zaposleni[index]={
        "ime": flask.request.form['ime'],
        "prezime": flask.request.form['prezime'],
        "datum": flask.request.form['datum'],
        "plata": int(flask.request.form['plata']),
        "rmesto": flask.request.form['rmesto'],
        "obrisan": False
    }
    return flask.redirect("/")

@app.route("/angazovanjeForma/<int:index>")
def angazovanjeForma(index):
    return flask.render_template("angazovanje.html", zaposleni=zaposleni[index], index=index)


@app.route("/angazuj/<int:index>", methods=["POST"])
def angazuj(index):
    angazovanja.append({
        "ime": zaposleni[index]["ime"],
        "naziv": flask.request.form["naziv"],
        "pocetak": flask.request.form["pocetak"],
        "kraj": flask.request.form["kraj"]
    })
    return flask.redirect("/")

#prikaz svih angazovanja
@app.route("/statistikaAngazovanja")
def statistika():
    return flask.render_template("statistikaAngazovanja.html", zaposleni=zaposleni, angazovanja=angazovanja)



#=========================
if __name__=="__main__":
    app.run("0.0.0.0", 5000, threded=True, debug=True)