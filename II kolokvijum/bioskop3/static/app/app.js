(function(angular){
    // Kreiranje modula pod nazivom app koji kao zavisnost ima ui router modul.
    // U slucaju da je modul vec kreiran, umesto kreiranja novog, bice
    // dobavljen postojeci modul.
    var app = angular.module("app", ["ui.router"]);

    app.config(["$stateProvider", "$urlRouterProvider", function($stateProvider, $urlRouterProvider) {
        // Stanje se registruje preko $stateProvider servisa, pozivom
        // metode state kojoj se prosledjuje state objekat.
        $stateProvider.state({
            name: "home", // Naziv stanja, mora biti zadat.
            url: "/", // URL kojim se prelazi na zadato stanje.
            templateUrl: "app/components/korisnici/korisnici.tpl.html", // URL do sablona koji se koristi za zadato stanje.
            controller: "korisniciCtrl", // Kontroler koji se vezuje za sablon u zadatom stanju.
            controllerAs: "pctrl" // Naziv vezanog kontrolera.
        })/*.state({
            name: "dodavanjeKorisnika",
            url: "/korisnikForma",
            templateUrl: "app/components/korisniciForma/korisnikForma.tpl.html",
            controller: "korisnikFormaCtrl",
            controllerAs: "pctrl"
        }).state({
            name: "dogadjaj",
            url: "/dogadjaji", 
            templateUrl: "app/components/dogadjaji/dogadjaji.tpl.html",
            controller: "dogadjajCtrl",
            controllerAs: "pctrl"
        }).state({
            name: "izmenaTipova",
            url: "/tipForma/{id}", // Ovaj URL je razlicit od URL-a za pristup svim proizvodima
                                    // jer sadrzi parametar id. Samo ukoliko se navede i parametar
                                    // dolazi do poklapanja sa ovim URL-om i do aktiviranja stanja.
            templateUrl: "app/components/tipoviForma/tipForma.tpl.html",
            controller: "tipFormaCtrl",
            controllerAs: "pctrl"
        }).state({
            name: "izmenaKorisnika",
            url: "/korisnikForma/{id}",
            templateUrl: "app/components/korisniciForma/korisnikForma.tpl.html",
            controller: "korisnikFormaCtrl", // Isti kontroler se moze koristiti vise puta.
            controllerAs: "pctrl"
        }).state({
            name: "dodavanjeTipova",
            url: "/tipForma",
            templateUrl: "app/components/tipoviForma/tipForma.tpl.html",
            controller: "tipFormaCtrl", // Isti kontroler se moze koristiti vise puta.
            controllerAs: "pctrl"
        }).state({
            name: "izmenaDogadjaja",
            url: "/dogadjaj/{id}",
            templateUrl: "app/components/dogadjaji/dogadjaji.tpl.html",
            controller: "dogadjajCtrl", // Isti kontroler se moze koristiti vise puta.
            controllerAs: "pctrl"
        });*/

        // Ukoliko zadati url ne pokazuje ni na jedno stanje
        // vrsi se preusmeravajne na url zadat pozivom metode
        // otherwise nad $urlRouterProvider servisom.
        $urlRouterProvider.otherwise("/")
    }])
})(angular);