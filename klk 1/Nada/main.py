import flask
from flask import   Flask

app = Flask(__name__, static_url_path="")

meni = [
    {
        "naziv": "Becka snicla",
        "tip": "glavno jelo",
        "kuhinja": "domaca",
        "cena": 450,
        "opis": "meso, krompir",
        "dostupnost": True
        
    },
    {
        "naziv": "Cazar",
        "tip": "salata",
        "kuhinja": "ditalijanska",
        "cena": 300,
        "opis": "feta, piletina, salata",
        "dostupnost": True
        
    },
    {
        "naziv": "Pihtije",
        "tip": "hladno predjelo",
        "kuhinja": "domaca",
        "cena": 250,
        "opis": "meso, zelatinasta masa",
        "dostupnost": True
        
    }
]

porucbine = []

@app.route("/")
def pocetna():
    return flask.render_template("index.html", meni = meni)

@app.route("/obrisi/<int:index>")
def obrisi(index):
    del meni[index]
    return flask.redirect("/")

@app.route("/dodavanjeForme")
def dodavanjeForme():
    return flask.render_template("/dodavanje.tpl.html")

@app.route("/dodaj", methods=["POST"])
def dodaj():
    meni.append({
        "naziv": flask.request.form['naziv'],
        "tip": flask.request.form['tip'],
        "kuhinja": flask.request.form['kuhinja'],
        "cena": int(flask.request.form['cena']),
        "opis": flask.request.form['opis'],
        "dostupnost":True
    })
    return flask.redirect("/")

@app.route("/izmeniFormu/<int:index>")
def izmeniFormu(index):
    return flask.render_template("izmena.tpl.html", meni=meni[index], index=index)

@app.route("/izmeni/<int:index>", methods=["POST"])
def izmena(index):
    meni[index]={
        "naziv": flask.request.form['naziv'],
        "tip": flask.request.form['tip'],
        "kuhinja": flask.request.form['kuhinja'],
        "cena": int(flask.request.form['cena']),
        "opis": flask.request.form['opis'],
        "dostupnost": True
    }
    return flask.redirect("/")

@app.route("/narucivanjeForma/<int:index>")
def narucivanjeForma(index):
    return flask.render_template("narucivanje.tpl.html", meni=meni[index], index=index)

@app.route("/naruci/<int:index>", methods=["POST"])
def naruci(index):
    porucbine.append({
        "naziv": meni[index]["naziv"],
        "kolicina": flask.request.form["kolicina"]
        
    })


if __name__=="__main__":
    app.run("0.0.0.0", 5000, threded=True, debug=True)
